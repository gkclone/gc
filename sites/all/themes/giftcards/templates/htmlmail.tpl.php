<style type="text/css" media="all">
  #page {
    width: 700px;
  }
  #header,
  #footer,
  #content {
    width: 100%;
  }
  #header td {
    text-align: center;
  }
  #footer {
    padding-top: 20px;
    padding-right: 20px;
    padding-bottom: 50px;
    padding-left: 20px;
    color: #656565;
  }
  #content__title {
    padding-top: 35px;
    padding-right: 20px;
    padding-bottom: 20px;
    padding-left: 20px;
    text-align: center;
    color: #005030;
    font-size: 20px;
    border-bottom: 1px solid #aaaaaa;
  }
  #content__content {
    padding-top: 20px;
    padding-right: 20px;
    padding-bottom: 35px;
    padding-left: 20px;
  }
  @import url();
</style>

<table cellpadding="0" cellspacing="0" id="page">
  <tr>
    <td>
      <table cellpadding="0" cellspacing="0" bgcolor="#ffffff" id="header">
        <tr>
          <td>
            <?php echo $logo; ?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table cellpadding="0" cellspacing="0" id="content">
        <tr>
          <td id="content__title">
            <?php echo $subject; ?>
          </td>
        </tr>
        <tr>
          <td id="content__content">
            <?php echo $body; ?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table cellpadding="0" cellspacing="0" bgcolor="#161817" id="footer">
        <tr>
          <td>
            Copyright <?php echo date('Y'); ?> Greene King. All rights reserved.
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
