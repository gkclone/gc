<?php

/**
 * Process variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function giftcards_process_html(&$variables, $hook) {
  // Add less settings.
  $less_settings = less_get_settings('minima');
  drupal_add_css(drupal_get_path('theme', 'giftcards') . '/less/giftcards.less', array('less' => $less_settings));
  drupal_add_css(drupal_get_path('theme', 'giftcards') . '/less/ie.less', array('less' => $less_settings, 'browsers' => array('IE' => 'lt IE 9', '!IE' => FALSE)));
  $variables['styles'] = drupal_get_css();
}

/**
 * Preprocess variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function giftcards_preprocess_html(&$variables, $hook) {
  drupal_add_css(libraries_get_path('selecter') . '/jquery.fs.selecter.css');
  drupal_add_js(libraries_get_path('selecter') . '/jquery.fs.selecter.min.js');

  if (gc_core_get_subdomain() == 'www') {
    global $is_https;
    $protocol = $is_https ? 'https' : 'http';
    drupal_add_js($protocol . '://fast.fonts.net/jsapi/952e5278-fb12-41f9-b628-f61586721711.js', 'external');
  }
}

/**
 * Implements hook_preprocess_panels_pane().
 */
function giftcards_preprocess_panels_pane(&$variables) {
  if ($variables['pane']->subtype == 'commerce_checkout_progress-indication') {
    $variables['theme_hook_suggestion'] = 'box__content_only';
  }
}

/**
 * Implements hook_less_variables_alter().
 */
function giftcards_less_variables_alter(&$variables) {
  $variables['base-font-size'] = '13';
  $variables['base-font-colour'] = '#656565';
  $variables['base-font-family'] = 'Georgia, "Times New Roman", Times, serif;';
  $variables['base-font-ratio'] = '1.16';

  $variables['base-gutter-width'] = '25';

  $variables['colour-primary'] = '#005030';
  //$variables['colour-primary-light'] = '#';
  //$variables['colour-primary-lighter'] = '#';
  //$variables['colour-primary-dark'] = '#';
  //$variables['colour-primary-darker'] = '#';

  $variables['colour-secondary'] = '#a89968';
  $variables['colour-secondary-dark'] = '#988852';

  $variables['colour-tertiary'] = '#9a9d5b';
  $variables['colour-tertiary-light'] = '#b9bb92';

  $variables['colour-neutral'] = '#8e8e8e';
  $variables['colour-neutral-light'] = '#b3b3b3';
  $variables['colour-neutral-lighter'] = '#c7c7c7';
  $variables['colour-neutral-lightest'] = '#eeeeee';
  $variables['colour-neutral-dark'] = '#656565';
  $variables['colour-neutral-darker'] = '#161817';

  $variables['headings-font-family'] = '"Gill Sans W01 Book", "brandon-grotesque", arial, sans-serif';
  $variables['headings-font-colour'] = '#8e8e8e';

  $variables['link-colour-hover'] = $variables['colour-secondary'];
}

/**
 * Implements preprocess_pane_content_header().
 */
function giftcards_preprocess_pane_content_header(&$variables) {
  // Disable breadcrumb.
  $variables['breadcrumb'] = '';

  // Hide title for menu nodes.
  if ($node = menu_get_object()) {
    if ($node->type == 'giftcard_display') {
      $variables['title'] = '';
    }
  }
}

/**
 * Preprocess variables for node.tpl.php.
 */
function giftcards_preprocess_node(&$variables) {
  if ($variables['node']->type == 'faq') {
    $variables['title_tag'] = 'h2';
    $variables['title_attributes_array']['class'][] = 'icon--double-angle-right';
  }
}

/**
 * Preprocess variables for theme_links().
 */
function giftcards_preprocess_links(&$variables) {
  if (isset($variables['links']['node-readmore'])) {
    $variables['links']['node-readmore']['attributes']['class'][] = 'button';
  }
}

/**
 * Implements hook_minima_layout_classes_alter().
 */
function giftcards_minima_layout_classes_alter(&$classes, $content) {
  //// Layout classes
  //$classes['primary_classes'] = '';
  //$classes['secondary_classes'] = ' desk--one-third';
  //$classes['tertiary_classes'] = ' desk--one-quarter desk--pull--three-quarters lap--one-third lap--pull--two-thirds';
  //
  //if ($content['secondary'] && $content['tertiary']) {
  //  $classes['primary_classes'] = ' desk--seven-twelfths';
  //}
  //elseif ($content['secondary'] && !$content['tertiary']) {
  //  $classes['primary_classes'] = ' desk--two-thirds';
  //}
  //elseif (!$content['secondary'] && $content['tertiary']) {
  //  $classes['primary_classes'] = ' desk--three-quarters desk--push--one-quarter lap--two-thirds lap--push--one-third';
  //}
}

/**
 * Process variables for gk-banners-slideshow.tpl.php
 */
function giftcards_preprocess_gk_banners_slideshow(&$variables) {
  drupal_add_js(libraries_get_path('stellar') . '/jquery.stellar.min.js', array('scope' => 'footer'));
  drupal_add_js('jQuery(document).ready(function () { jQuery.stellar(); });', array('type' => 'inline', 'scope' => 'footer', 'weight' => 5));

  $variables['attributes_array']['data-stellar-offset-parent'] = 'true';
  $variables['attributes_array']['data-stellar-horizontal-offset'] = '50%';
}

/**
 * Process variables for gk-banners-slide.tpl.php
 */
function giftcards_preprocess_gk_banners_slide(&$variables) {
  $variables['attributes_array']['data-stellar-background-ratio'] = 0.5;
  $variables['attributes_array']['data-stellar-vertical-offset'] = -200;
  $variables['attributes_array']['data-cycle-slide-css'] = '{"background-image" : "url(' . image_style_url('banner', $variables['image']['#path']) .')" }';
  $variables['image'] = '';
}

/**
 * Implements hook_form_alter().
 */
function giftcards_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'commerce_checkout_form_shipping') {
    $form['#attributes']['class'][] = 'grid';
  }

  $checkout_form_ids = array(
    'commerce_checkout_form_checkout',
    'commerce_checkout_form_shipping',
  );

  if (in_array($form_id, $checkout_form_ids)) {
    foreach (commerce_checkout_panes(array('enabled' => TRUE, 'page' => $form_state['checkout_page']['page_id'])) as $pane_id => $checkout_pane) {
      if (isset($form[$pane_id])) {
        $attributes = array('class' => array('grid__cell'));

        switch ($pane_id) {
          case 'gc_checkout_summary_checkout':
          case 'gc_checkout_summary_shipping':
            $attributes['class'][] = 'desk--one-third';
            $attributes['class'][] = 'desk--push--two-thirds';

            if ($pane_id == 'gc_checkout_summary_checkout') {
              //$form['#attached']['js'][] = drupal_get_path('module', 'gc_checkout') . '/theme/js/gc_checkout.copytorecipient.js';
            }
            break;

          case 'account':
            $attributes['class'][] = 'desk--two-thirds';
            $attributes['class'][] = 'desk--pull--one-third';
            break;

          case 'customer_profile_billing':
            $attributes['class'][] = 'desk--one-third';
            $attributes['class'][] = 'desk--pull--one-third';
            break;

          case 'customer_profile_shipping':
            $attributes['class'][] = 'desk--one-third';
            $attributes['class'][] = 'desk--pull--one-third';
            break;

          case 'commerce_shipping':
            $attributes['class'][] = 'desk--two-thirds';
            $attributes['class'][] = 'desk--pull--one-third';
            break;
        }

        $form[$pane_id]['#prefix'] = '<div' . drupal_attributes($attributes) . '>';
        $form[$pane_id]['#suffix'] = '</div>';
      }
    }

    if (isset($form['buttons'])) {
      $attributes = array('class' => array('grid__cell'));

      $form['buttons']['#prefix'] = '<div' . drupal_attributes($attributes) . '>';
      $form['buttons']['#suffix'] = '</div>';
    }
  }
}

/**
 * Implements hook_form_alter()
 */
function giftcards_form_gk_contact_form_alter(&$form, &$form_state) {
  $fields_to_hide = array('address_1', 'address_2', 'town', 'county', 'postcode', 'telephone');

  $form['title'] = array(
    '#markup' => '<h2>' . t('Send us a message') . '</h2>',
    '#weight' => -100,
  );

  foreach($form['contact'] as $key => &$element) {
    if (!empty($element['#title'])) {
      if (in_array($element['#type'], array('textfield', 'textarea'))) {
        $element['#attributes']['placeholder'] = $element['#title'];
        $element['#title_display'] = 'hidden';
      }
      elseif ($element['#type'] == 'select') {
        $element['#empty_option'] = '- ' . $element['#title'] . ' -';
        $element['#title_display'] = 'hidden';
      }

      if (in_array($key, $fields_to_hide)) {
        $element['#access'] = FALSE;
      }
    }

    if (!empty($element['#required'])) {
      $element['#field_suffix'] = '<span class="is-required">*</span>';
    }
  }

  $form['contact']['required_info'] = array(
    '#markup' => '<div class="required_info"><span class="is-required">*</span> ' . t('Indicates a required field') . '</div>',
    '#weight' => 100,
  );

  if ($telephone = variable_get('gk_contact_address_telephone')) {
    $form['alternative_contact'] = array(
      '#markup' => '<div class="alternative_contact">' . t('If you would prefer to speak to a member of our team call us on... ') . '<span class="telephone">' . $telephone . '</span></div>',
      '#weight' => 200,
    );
  }
}

/**
 * Preprocess variables for theme_form().
 */
function giftcards_form($variables) {
  $element = $variables['element'];
  if (isset($element['#action'])) {
    $element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
  }
  element_set_attributes($element, array('method', 'id'));
  if (empty($element['#attributes']['accept-charset'])) {
    $element['#attributes']['accept-charset'] = "UTF-8";
  }

  $wrapper_attributes = array();

  $grid_forms = array(
    'commerce_checkout_form_checkout',
  );

  if (in_array($variables['element']['#form_id'], $grid_forms)) {
    $wrapper_attributes = array(
      'class' => array('grid'),
    );
  }

  // Anonymous DIV to satisfy XHTML compliance.
  return '<form' . drupal_attributes($element['#attributes']) . '><div' . drupal_attributes($wrapper_attributes) . '>' . $element['#children'] . '</div></form>';
}

/**
 * Preprocess variables for gk-cookies-disclaimer.tpl.php
 */
function giftcards_preprocess_gk_cookies_disclaimer(&$variables) {
  // Add icon class to continue link.
  $variables['continue_link'] = l('', request_path(), array(
    'attributes' => array('class' => array('icon--double-angle-up'))
  ));
}

/**
 * Implements hook_filter_default_formats_alter().
 */
function giftcards_filter_default_formats_alter(&$formats) {
  // Make it possible to add elements with classes like 'gk-*'.
  if (!empty($formats['full_html'])) {
    $formats['full_html']['filters']['wysiwyg']['settings']['rule_valid_classes'][] = 'gk-*';
  }
}
