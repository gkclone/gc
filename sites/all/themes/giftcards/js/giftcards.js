(function($) {

  Drupal.giftcards = {};

  Drupal.giftcards.tableRespond = function() {
    if (Modernizr.mq('only screen and (max-width: 1024px)')) {
      $('table').addClass('stacked').not('.processed').addClass('processed').each(function() {
        $table = $(this);

        $table.find('tbody').each(function(){
          var $tbody = $(this);

          $tbody.find('tr').each(function(){
            var $tr = $(this);

            $tr.find('td, th').each(function(){
              var horizontal_adjust = 0;

              if ($(this).attr('colspan') > 1) {
                horizontal_adjust = $(this).attr('colspan') - 1;
              }

              var label = $table.find('th').eq($(this).index() + horizontal_adjust).text();

              $(this).attr('data-th', label);

              if ($(this).attr('rowspan') > 1 && !Drupal.giftcards.tableResponded) {
                var extend_rows = $(this).attr('rowspan') - 1;

                var tdBeforeIndex = 0;

                if ($(this).index() > 0) {
                  tdBeforeIndex = $(this).index() - 1;
                }

                for (var i = 1; i <= extend_rows; i++) {
                  $tbody
                    .find('tr')
                    .eq($tr.index() + i)
                    .find('td')
                    .eq(tdBeforeIndex)
                    .after($(this).clone().removeAttr('rowspan').addClass('is-ghost-td'));
                }
              }
            });
          });
        });
      });

      $('.is-ghost-td').show();
    }
    else {
      $('table.stacked').removeClass('stacked');
      $('.is-ghost-td').hide();

      Drupal.giftcards.tableResponded = false;
    }
  }

  Drupal.behaviors.giftcards = {
    attach: function (context, settings) {
      // Style select form elements (excluding the checkout form (for now)).
      $('select').filter(function() {
        return !$(this).parents('form').is('#commerce-checkout-form-checkout');
      }).selecter();

      // Add icon to accordion headings.
      var accordion_headings = $('.accordion__heading');
      accordion_headings.first().addClass('icon--double-angle-down');
      accordion_headings.click(function() {
        accordion_headings.addClass('icon--double-angle-right').removeClass('icon--double-angle-down');
        $(this).removeClass('icon--double-angle-right').addClass('icon--double-angle-down');
      });

      Drupal.giftcards.tableRespond();

      $(window).resize(function(){
        Drupal.giftcards.tableRespond();
      })
    }
  }

})(jQuery);
