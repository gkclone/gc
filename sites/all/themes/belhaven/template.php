<?php

/**
 * Process variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function belhaven_process_html(&$variables, $hook) {
  // Add less settings.
  $less_settings = less_get_settings('minima');
  drupal_add_css(drupal_get_path('theme', 'belhaven') . '/less/belhaven.less', array('less' => $less_settings));
  $variables['styles'] = drupal_get_css();
}

/**
 * Implements hook_less_variables_alter().
 */
function belhaven_less_variables_alter(&$variables) {
  $variables['base-font-size'] = '14';
  $variables['base-font-colour'] = '#656565';
  $variables['base-font-family'] = '"Helvetica Neue", Helvetica, Arial, sans-serif';
  $variables['base-font-ratio'] = '1.16';

  $variables['base-gutter-width'] = '25';

  $variables['colour-primary'] = '#193675';
  $variables['colour-primary-light'] = '#91afd1';
  $variables['colour-primary-lighter'] = '#bcccdb';
  $variables['colour-primary-dark'] = '#0e194c';
  //$variables['colour-primary-darker'] = '#';

  $variables['colour-secondary'] = '#be9635';
  $variables['colour-secondary-dark'] = '#988852';

  $variables['colour-tertiary'] = '#a2a569';

  $variables['colour-neutral'] = '#8e8e8e';
  $variables['colour-neutral-light'] = '#b3b3b3';
  $variables['colour-neutral-lighter'] = '#c7c7c7';
  $variables['colour-neutral-lightest'] = '#eeeeee';
  $variables['colour-neutral-dark'] = '#656565';
  $variables['colour-neutral-darker'] = '#161817';

  $variables['headings-font-family'] = '"Gill Sans", "Gill Sans MT", Calibri, sans-serif';
  $variables['headings-font-colour'] = '#1b2a6b';

  $variables['link-colour-hover'] = $variables['colour-primary-dark'];
}
