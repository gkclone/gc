$(function() {
  // Add checkout progress to page.
  var checkout_progress =
    '<ol class="commerce-checkout-progress clearfix inline">' +
      '<li class="checkout visited first">Delivery information</li>' +
      '<li class="next visited shipping">Delivery method</li>' +
      '<li class="active payment">Payment</li>' +
      '<li class="complete last">Checkout complete</li>' +
    '</ol>' +
    '<h1 id="page-title">Payment</h1>';
  $('#content-internal').prepend(checkout_progress);

  var bodyclass ='gk';

  var regex = /[\/^]{2}(.+)\.(gkgiftcards|giftcards\.greenekingdigital|giftcards\.local)/i;
  var matches;

  if (matches = regex.exec(document.referrer)) {
    bodyclass = matches[1];

    if (bodyclass == 'www') {
      bodyclass = 'gk';
    }

    $.cookie('gc_bodyclass', bodyclass);
  }
  else if (typeof $.cookie('gc_bodyclass') != 'undefined') {
    bodyclass = $.cookie('gc_bodyclass');
  }

  $('body').addClass(bodyclass);
});
