<?php

/**
 * Process variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function lochfyne_process_html(&$variables, $hook) {
  // Add less settings.
  $less_settings = less_get_settings('minima');
  drupal_add_css(drupal_get_path('theme', 'lochfyne') . '/less/lochfyne.less', array('less' => $less_settings));
  $variables['styles'] = drupal_get_css();
}

/**
 * Implements hook_less_variables_alter().
 */
function lochfyne_less_variables_alter(&$variables) {
  $variables['base-font-size'] = '16';
  $variables['base-font-colour'] = '#656565';
  $variables['base-font-family'] = '"freight-sans-pro", arial, sans-serif';
  $variables['base-font-ratio'] = '1.16';

  $variables['base-gutter-width'] = '25';

  $variables['colour-primary'] = '#1c2a6a';
  $variables['colour-primary-light'] = '#91afd1';
  $variables['colour-primary-lighter'] = '#c0c8d5';
  $variables['colour-primary-dark'] = '#0e194c';
  //$variables['colour-primary-darker'] = '#';

  $variables['colour-secondary'] = '#a89968';
  $variables['colour-secondary-dark'] = '#988852';

  $variables['colour-tertiary'] = '#a2a569';

  $variables['colour-neutral'] = '#8e8e8e';
  $variables['colour-neutral-light'] = '#b3b3b3';
  $variables['colour-neutral-lighter'] = '#c7c7c7';
  $variables['colour-neutral-lightest'] = '#eeeeee';
  $variables['colour-neutral-dark'] = '#656565';
  $variables['colour-neutral-darker'] = '#161817';

  $variables['headings-font-family'] = '"futura-pt", arial, sans-serif';
  $variables['headings-font-colour'] = '#1b2a6b';

  $variables['link-colour-hover'] = $variables['colour-primary-dark'];
}

/**
 * Implements hook_preprocess_panels_panel().
 */
function lochfyne_preprocess_panels_pane(&$variables) {
  // Add parallax to buy now cta on homepage.
  if ($variables['pane']->subtype = 'gc_product-gc_product_buy_now_cta') {
    drupal_add_js(libraries_get_path('stellar') . '/jquery.stellar.min.js', array('scope' => 'footer'));
    drupal_add_js('jQuery(document).ready(function () { jQuery.stellar(); });', array('type' => 'inline', 'scope' => 'footer', 'weight' => 5));
    $variables['attributes_array']['data-stellar-background-ratio'] = '0.5';
    $variables['attributes_array']['data-stellar-horizontal-offset'] = '50%';
    $variables['attributes_array']['data-stellar-vertical-offset'] = '-360';
  }
}
