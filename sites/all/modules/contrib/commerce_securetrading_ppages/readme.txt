-------------
1.1. Contents
-------------

1.1. Contents
1.2. About
2.1. Installation
2.2. Configuration
2.3. Notification
2.4. Redirect
2.5. Site Security
2.6. Notification Hash
2.7. Expected Behaviour
3.1. Support

----------
1.2. About
----------

This SecureTrading Drupal Commerce Payment Pages module has been developed for the following specifications:

Drupal Commerce Version: 7.x-1.5
PHP Version: 5.0

Module Version: 2.5.0

-----------------
2.1. Installation
-----------------

Installation of the plugin is extremely simple.

1 - Copy and paste the contents of the www/ folder into your root Drupal Commerce installation.
2 - If you are prompted with a box asking you if you would like to merge the folders, click 'Yes'.
3 - Login to your administration area.
4 - Click on 'Modules'.
5 - Check the box to the right of 'SecureTrading Payment Pages' and then click 'Save configuration'.

The shopping cart should now be installed and ready to configure.

-------------------
2.2. Configuration
-------------------

To configure the plugin you should perform the following steps:

1 - Login to your adminstration area.
2 - Click on 'Configure' to the right of 'SecureTrading Payment Pages'.
3 - A list of configuration options will appear.  Fill in these options as desired and then click on 'Save configuration'.

Module Name - This is the name of this payment module that will be displayed to your customers in the checkout.

Notification Hash - This is the notification hash.  This option is discussed further in section 2.6.

Settle Due Date - This is the number of days you wish to wait before retrieving funds from your customers' account.  The default value is "Process Immediately".

Settle Status - This is the settlement status that will be applied to all successful transactions.  The default status is "0".

Site Reference - This is the Site Reference provided to you by SecureTrading.  If you do not have a Site Reference then please contact our Support department on +44 (0) 1248 672 050

Site Security - This is the Site Security hash.  This option is discussed further in section 2.5.

Use Notification Hash - Select this option to enable the notification hash.  This option is discussed further in section 2.6.

Use Site Security - Select this option to enable the Site Security hash.  This option is discussed further in section 2.5.

------------------
2.3.  Notification
------------------

You must set-up  a notification in MyST before this plugin can work correctly.

This feature is responsible for updating order information in the Drupal Commerce back-end after payment has been made via the SecureTrading Payment Gateway.

To set-up a notification you should perform the following steps:

1 - Login to MyST and select "Notifications".
2 - Select 'Add Filter' and then enter the following information:

FILTERS

  Description:
     Enter a recognizable name of your choice here.

  Requests:
     Auth

  Payment Types:
     Select all card types that you will be accepting here.

  Error Codes:
     0

3 - Click 'Save'.
4 - Select 'Add Destination and enter the following information:

DESTINATIONS

  Description:
     Enter a recognizable name of your choice here.

  Notification Type:
     URL

  Process Notification:
     Online

  Destination:
    <your_root_drupal_commerce_install_here>/commerce/securetrading_ppages/notification

  Security Password:
     This is the field that you will enter your chosen notification hash into.  See section 2.6 for more information.

  Security Algorithm:
     sha256

  Fields:
     errorcode
     orderreference
     transactionreference

  Custom Fields:
     None

5 - Click 'Save'.
6 - Select the filter and destination you have just created from the two initially blank select menus (under 'Current Notifications').
7 - Click 'Save'.

The notification will now have been enabled.

-------------
2.4. Redirect
-------------

You must set-up a redirect before this plugin will work correctly.

Please complete the Redirect Form and email it to our support department at support@securetrading.com

The Redirect Form can be downloaded here: http://securetrading.com/sites/default/files/downloads/ppages/STPP_Redirect_Request_Form.doc

The following information should be inserted into the redirect form:

Redirect URL:
    <your_root_drupal_commerce_install_here>/commerce/securetrading_ppages/redirect

Fields:
     orderreference

Custom Fields:
     None

------------------
2.5. Site Security
------------------

A security code will prevent malicious users from modifying sensitive payment information before being directed to the payment screen.

This feature can be enabled by following these steps:

1 - Login to your adminstration area.
2 - Click on 'Configure' to the right of 'SecureTrading Payment Pages'.
3 - A list of configuration options will appear.
4 - Select 'Yes' in the select box below 'Use Site Security'.
5 - Click on 'Save configuration'.
6 - Enter a hard to guess combination of letters and numbers into the 'Site Security Password' field.  This combination should be at least 8 characters long.
7 - Click 'save'.
8  - You must now contact Support by emailing support@securetrading.com.  Inform them that you have "enabled the Site Security Password Hash".  When prompted for a list of "enabled fields", you must tell them
the following, in this order:

     currencyiso3a
     mainamount
     sitereference
     settlestatus
     settleduedate
     PASSWORD *

'Site Security' is now enabled.  Remember to never tell any other individuals your Site Security Password.  Do not store hard copies of this password anywhere.

* The last field, 'PASSWORD', is to be the combination of characters you entered into the 'Site Security Password'.

----------------------
2.6. Notification Hash
----------------------

It is highly recommended that you enable the notification hash.  See section 2.3 for more information on the notification script itself.

To enable the notification hash you will firstly have to enter a 'Security Password' in the 'Destination' window of the MyST Notification page (see section 2.3).
You will also have to enter the same password into the Drupal Commerce Payment Pages configuration page.  Details on how to configure the module were given in section 2.2.

-----------------------
2.7. Expected Behaviour
-----------------------

When payment has been taken successfully all rules for 'commerce_checkout_complete' will be run.

In a default installation of DrupalCommerce this means that incomplete orders will be set to the 'Checkout: Payment' status as a 'Shopping Cart' and complete (paid) orders will be set to the 'Pending' status as an 'Order'.

------------
3.1. Support
------------

If you require any assistance then please contact us immediately.

When contacting our Support department you should search the www/sites/all/modules/commerce_securetrading_ppages/ directory for a logs/log.txt file.  If this file exists then please submit it with your initial support request.

Please also send us any server access/error logs along with as much information as you can regarding your problem.

http://www.securetrading.com/contact.html
support@securetrading.com
