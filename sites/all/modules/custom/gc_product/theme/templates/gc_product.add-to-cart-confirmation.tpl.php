<div class="gc_product-add-to-cart-confirmation" title="Item successfully added to your basket">
  <h2>Item successfully added to basket</h2>
  <?php if ($image): ?>
    <?php echo render($image); ?>
  <?php endif; ?>

  <div class="fields">
    <?php foreach ($fields as $field): ?>
      <div class="fields__field">
        <div class="field__label"><?php echo $field['label']; ?></div>
        <div class="field__value"><?php echo render($field['value']); ?></div>
      </div>
    <?php endforeach; ?>
  </div>
</div>
