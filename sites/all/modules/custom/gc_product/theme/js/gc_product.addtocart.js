(function($) {

  Drupal.behaviors.gc_product_addtocart = {
    attach: function (context, settings) {
      // Dialog for add to cart box
      $('.gc_product-add-to-cart-confirmation').dialog({
        resizable: false,
        draggable: false,
        position: 'center',
        height: 'auto',
        width: 550,
        modal: true,
        buttons: {
         "Continue shopping": function() {
           $(this).dialog("close");
         },
         "Proceed to checkout": function() {
           document.location.href = '/cart/';
         }
        }
      });

      // For 'other' amount that a user can enter for the gift card, pressing enter should
      // lose focus rather than submit the form. On blur will take care of re-hiding
      // textbox and replace text with their chosen value

      // 'Other' label
      $other_label = $('#edit-line-item-fields-giftcard-value-und-select-select-or-other + label')

      // 'Other' input
      $select_or_other = $('#edit-line-item-fields-giftcard-value-und-select-select-or-other');

      // Get default label val
      var other_label_text = $('#edit-line-item-fields-giftcard-value-und-select-select-or-other + label').text();

      $('.select-or-other-other')
      .keydown(function(e) {
        var code = e.which;

        if (code == 13) {
          $(this).blur();
          e.preventDefault();
        }
      })
      .blur(function() {
        var value = $(this).val();

        if ($.isNumeric(value)) {
          if ($select_or_other.is(':checked')) {
            $(this).parents('.form-item').first().hide(0);
          }

          $other_label.addClass('is-populated').text(value);
        }
        else {
          $other_label.removeClass('is-populated').text(other_label_text);
        }
      })
      // Take care of hiding text behind it whilst focused
      .focus(function(){
        $other_label.text('.');
      });

      if ($select_or_other.length > 0) {
        jQuery.fx.off = !$.fx.off;
      }
    }
  }

})(jQuery);
