<?php
/**
 * @file
 * gc_product.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function gc_product_default_rules_configuration() {
  $items = array();
  $items['rules_gc_product_calculate_price'] = entity_import('rules_config', '{ "rules_gc_product_calculate_price" : {
      "LABEL" : "GC Product: Calculate price",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "gc" ],
      "REQUIRES" : [ "rules", "commerce_line_item", "commerce_product_reference" ],
      "ON" : [ "commerce_product_calculate_sell_price" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-line-item" ], "field" : "giftcard_value" } }
      ],
      "DO" : [
        { "data_calc" : {
            "USING" : {
              "input_1" : [ "commerce-line-item:giftcard-value" ],
              "op" : "*",
              "input_2" : "100"
            },
            "PROVIDE" : { "result" : { "giftcard_value" : "Gift card value" } }
          }
        },
        { "commerce_line_item_unit_price_add" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : [ "giftcard-value" ],
            "component_name" : "base_price",
            "round_mode" : "1"
          }
        }
      ]
    }
  }');
  return $items;
}
