<?php

/**
 * @file
 *
 * Theming functions for the products module.
 */

/**
 * Preprocess variables for the gc_product.add-to-cart-confirmation template.
 */
function template_preprocess_gc_product_add_to_cart_confirmation(&$variables) {
  $giftcard_display = $variables['giftcard_display'];
  $line_item = $variables['line_item'];

  $variables['image'] = field_view_field('node', $giftcard_display, 'field_giftcard_display_image', array(
    'label' => 'hidden',
    'settings' => array(
      'image_style' => 'gc_product_thumb_small',
    ),
  ));

  $labels = array(
    'giftcard' => t('Gift Card'),
    'message' => t('Message'),
    'quantity' => t('Quantity'),
    'value' => t('Value'),
  );

  if (!$message = field_view_field('commerce_line_item', $line_item, 'giftcard_message', array('label' => 'hidden'))) {
    unset($labels['message']);
  }

  $fields = array();

  foreach ($labels as $key => $label) {
    $value = '';

    switch ($key) {
      case 'giftcard':
        $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
        $value = $wrapper->commerce_product->value()->title;
      break;

      case 'message':
        $value = $message ? $message : '';
      break;

      case 'quantity':
        $value = $line_item->quantity;
      break;

      case 'value':
        $value = $line_item->giftcard_value[LANGUAGE_NONE][0]['value'];
        $value = commerce_currency_format($value * 100, 'GBP');
      break;
    }

    if (!empty($value)) {
      $fields[$key] = array(
        'label' => $label,
        'value' => $value,
      );
    }
  }

  $variables['fields'] = $fields;
}

/**
 * Theme a bunch of products.
 */
function theme_gc_product_list($variables) {
  $output = '';

  if (!empty($variables['items'])) {
    $build = node_view_multiple($variables['items']);

    $build['nodes']['#theme_wrappers'] = array('container');
    $build['nodes']['#attributes']['class'] = array('grid products');

    foreach (element_children($build['nodes']) as $nid) {
      $build['nodes'][$nid]['#theme_wrappers'] = array('container');
      $build['nodes'][$nid]['#attributes']['class'] = array(
        'products__product',
        'grid__cell',
        'desk--one-quarter',
        'lap--one-half',
      );
    }

    $output = drupal_render($build);
  }

  return $output;
}
