<?php

/**
 * Preprocess function for the gc_checkout.summary template.
 */
function template_preprocess_gc_checkout_summary(&$variables) {
  $order = $variables['order'];
  $line_item_ids = array();

  foreach ($order->commerce_line_items[LANGUAGE_NONE] as $line_item_id) {
    $line_item_ids[] = $line_item_id['line_item_id'];
  }

  $line_items = commerce_line_item_load_multiple($line_item_ids);

  foreach ($line_items as $line_item) {
    if ($line_item->type != 'giftcard') {
      continue;
    }

    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    $product_id = $line_item_wrapper->commerce_product->product_id->value();

    $giftcard_display_items = gc_product_get_items(array(
      'filters' => array(
        'product_id' => $product_id,
      ),
    ));

    if (!$giftcard_display_items) {
      continue;
    }

    $giftcard_display = reset($giftcard_display_items);

    // Find an image for this line item.
    $image = NULL;

    if ($image_field = field_get_items('node', $giftcard_display, 'field_giftcard_display_image')) {
      $image = theme('image_style', array(
        'path' => $image_field[0]['uri'],
        'style_name' => 'gc_product_thumb_large',
      ));
    }

    // Build an array of fields (name, amount, quantity, total).
    $line_item_total = $line_item->commerce_total[LANGUAGE_NONE][0];
    $line_item_unit_value = $line_item->giftcard_value[LANGUAGE_NONE][0];

    $fields = array(
      'card' => array(
        'label' => t('Card'),
        'value' => $line_item_wrapper->commerce_product->value()->title,
      ),
      'amount' => array(
        'label' => t('Amount'),
        'value' => commerce_currency_format($line_item_unit_value['value'] * 100, $line_item_total['currency_code']),
      ),
      'quantity' => array(
        'label' => t('Quantity'),
        'value' => $line_item->quantity,
      ),
      'total' => array(
        'label' => t('Total'),
        'value' => commerce_currency_format($line_item_total['amount'], $line_item_total['currency_code']),
      ),
    );

    $variables['line_items'][$line_item->line_item_id] = array(
      'image' => $image,
      'fields' => $fields,
    );
  }

  // Shipping address.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  if ($customer_profile = $order_wrapper->commerce_customer_shipping->value()) {
    if ($address = field_get_items('commerce_customer_profile', $customer_profile, 'commerce_customer_address')) {
      $variables['shipping_address'] = field_view_field('commerce_customer_profile', $customer_profile, 'commerce_customer_address', array(
        'label' => 'hidden',
      ));
    }
  }

  // Order total.
  $order_total = $order->commerce_order_total[LANGUAGE_NONE][0];
  $variables['order_total'] = commerce_currency_format($order_total['amount'], $order_total['currency_code']);
}
