<?php

/**
 * @file
 * Code for the Checkout feature.
 */

include_once 'gc_checkout.features.inc';
include_once 'includes/gc_checkout.theme.inc';

/**
 * Implements hook_menu().
 */
function gc_checkout_menu() {
  $items['js/gc_checkout/summary'] = array(
    'page callback' => 'gc_checkout_summary_js',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['js/gc_checkout/progress'] = array(
    'page callback' => 'gc_checkout_progress_js',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function gc_checkout_theme($existing, $type, $theme, $path) {
  return array(
    'gc_checkout_summary' => array(
      'variables' => array('order' => NULL),
      'template' => 'gc_checkout.summary',
      'path' => $path . '/theme/templates',
    ),
  );
}

/**
 * Implements hook_theme_registry_alter().
 */
function gc_checkout_theme_registry_alter(&$theme_registry) {
  if (isset($theme_registry['commerce_checkout_errors_message'])) {
    $theme = &$theme_registry['commerce_checkout_errors_message'];

    $theme['path'] = drupal_get_path('module', 'gc_checkout') . '/theme/templates';
    $theme['template'] = 'gc_checkout.commerce-checkout-errors-message';
  }
}

/**
 * Implements hook_block_info_alter().
 */
function gc_checkout_block_info_alter(&$blocks, $theme, $code_blocks) {
  // Disable commerce checkout progress block in block system.
  // It gets added by the page manager site template.
  $blocks['commerce_checkout_progress']['indication']['status'] = 0;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function gc_checkout_form_commerce_checkout_form_checkout_alter(&$form, &$form_state, $form_id) {
  // Hide the email fields label and set it's placeholder.
  if (!empty($form['account']['login']['mail'])) {
    $form['account']['login']['mail']['#title_display'] = 'hidden';
    $form['account']['login']['mail']['#attributes']['placeholder'] = t('Email address');

    if (!empty($form['account']['login']['mail']['#required'])) {
      $form['account']['login']['mail']['#field_suffix'] = '<span class="is-required">*</span>';
    }
  }

  // Set placeholder attribute for input elements in the customer profile forms
  // and set the label display types to 'hidden'.
  foreach (array('billing', 'shipping') as $profile) {
    $profile_element = &$form["customer_profile_$profile"];
    $profile_element["field_{$profile}_title"][LANGUAGE_NONE]['#title_display'] = 'hidden';

    if (!empty($profile_element["field_{$profile}_title"][LANGUAGE_NONE]['#required'])) {
      $profile_element["field_{$profile}_title"][LANGUAGE_NONE]['#field_suffix'] = '<span class="is-required">*</span>';
    }

    foreach (array('first_name', 'last_name') as $field) {
      $element = &$profile_element["field_{$profile}_$field"][LANGUAGE_NONE][0]['value'];
      $element['#attributes']['placeholder'] = $element['#title'];

      $element['#title_display'] = 'hidden';

      if (!empty($element['#required'])) {
        $element['#field_suffix'] = '<span class="is-required">*</span>';
      }
    }

    $address_element[$profile] = &$profile_element['commerce_customer_address'][LANGUAGE_NONE][0];
    $address_element[$profile]['country']['#title_display'] = 'hidden';

    foreach (array('street_block', 'locality_block') as $block) {
      foreach (element_children($address_element[$profile][$block]) as $key) {
        $address_field_element = &$address_element[$profile][$block][$key];

        $address_field_element['#attributes']['placeholder'] = $address_field_element['#title'];
        $address_field_element['#title_display'] = 'hidden';

        if (!empty($address_field_element['#required'])) {
          $address_field_element['#field_suffix'] = '<span class="is-required">*</span>';
        }
      }
    }
  }

  // Communications preferences field labels.
  $form['commerce_user_profile_pane']['field_user_comms_opt_in_brand'][LANGUAGE_NONE]['#title'] = t('Please tick this box if you do not wish to receive offers and information from the Greene King Group from time to time by email that may be of interest.');
  $form['commerce_user_profile_pane']['field_user_comms_opt_in_other'][LANGUAGE_NONE]['#title'] = t('If you wish to receive offers from carefully selected 3rd parties then please tick here.');

  // Required fields marker.
  $form['commerce_user_profile_pane']['required_info'] = array(
    '#markup' => '<div class="required_info"><span class="is-required">*</span> ' . t('Indicates a required field') . '</div>',
    '#weight' => +100,
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function gc_checkout_form_commerce_checkout_form_shipping_alter(&$form, &$form_state, $form_id) {
  foreach ($form['commerce_shipping']['shipping_rates']['#value'] as $rate) {
    // Add shipping rate descriptions to radio buttons.
    $data = $rate->data['shipping_service'];

    $form['commerce_shipping']['shipping_service'][$data['name']] = array(
      '#description' => $data['description'],
    );

    // Prevent an AJAX request from being triggered when the user selects a
    // different delivery method, since this doesn't seem to be required.
    unset($form['commerce_shipping']['shipping_service']['#ajax']);

    // Build a list of flat rate services keyed by cost (for ordering below).
    $flat_rate_services_by_cost[$data['base_rate']['amount']] = $data['name'];
  }

  // Order flat rate services by cost ascending.
  if (!empty($flat_rate_services_by_cost)) {
    $new_options = array();
    $old_options = &$form['commerce_shipping']['shipping_service']['#options'];

    ksort($flat_rate_services_by_cost);

    foreach ($flat_rate_services_by_cost as $service) {
      $new_options[$service] = $old_options[$service];
    }

    $old_options = $new_options;

    // Reset the default value of the element to the first radio in the field.
    $form['commerce_shipping']['shipping_service']['#default_value'] = reset($flat_rate_services_by_cost);
  }

  // Flat rate service label.
  $form['commerce_shipping']['shipping_service']['#title'] = t('Select your delivery preference from the options below:');

  // Add a notice to the bottom of the form.
  $form['commerce_shipping']['notice'] = array(
    '#type' => 'markup',
    '#markup' => t('Please note: We are not liable for any cards lost in the delivery process.'),
    '#theme_wrappers' => array('container'),
    '#weight' => +100,
  );
}

/**
 * Implements hook_commerce_checkout_pane_info().
 */
function gc_checkout_commerce_checkout_pane_info() {
  $checkout_panes['gc_checkout_summary_checkout'] = array(
    'title' => t('Summary: Checkout'),
    'base' => 'gc_checkout_summary_pane',
    'page' => 'checkout',
  );

  $checkout_panes['gc_checkout_summary_shipping'] = array(
    'title' => t('Summary: Shipping'),
    'base' => 'gc_checkout_summary_pane',
    'page' => 'shipping',
  );

  $checkout_panes['gc_checkout_google_analytics_pane'] = array(
    'title' => t('Google analytics'),
    'base' => 'gc_checkout_google_analytics_pane',
    'page' => 'complete',
    'fieldset' => FALSE,
    'weight' => 10,
  );

  return $checkout_panes;
}

/**
 * Implements hook_commerce_checkout_pane_info_alter().
 */
function gc_checkout_commerce_checkout_pane_info_alter(&$checkout_panes) {
  // We don't use the following panes so just remove them.
  unset(
    $checkout_panes['cart_contents'],
    $checkout_panes['checkout_review'],
    $checkout_panes['commerce_payment_redirect']
  );

  // Delivery information page.
  if (arg(0) != 'admin') {
    $pane_count_start = 1;

    if (user_is_anonymous()) {
      $pane_count_start = 2;
      $checkout_panes['account']['title'] = t('1. Account details');
    }

    $checkout_panes['customer_profile_billing']['title'] = t('!number. Cardholder details', array('!number' => $pane_count_start));
    $checkout_panes['customer_profile_shipping']['title'] = t('!number. Delivery details', array('!number' => $pane_count_start + 1));

    $checkout_panes['gc_checkout_summary_checkout']['title'] = t('Summary');
    $checkout_panes['commerce_user_profile_pane']['title'] = '';
  }

  $checkout_panes['gc_checkout_summary_checkout']['weight'] = 0;
  $checkout_panes['gc_checkout_summary_checkout'][  'page'] = 'checkout';
  $checkout_panes['account'                     ]['weight'] = 1;
  $checkout_panes['account'                     ][  'page'] = 'checkout';
  $checkout_panes['customer_profile_billing'    ]['weight'] = 2;
  $checkout_panes['customer_profile_billing'    ][  'page'] = 'checkout';
  $checkout_panes['customer_profile_shipping'   ]['weight'] = 3;
  $checkout_panes['customer_profile_shipping'   ][  'page'] = 'checkout';
  $checkout_panes['commerce_user_profile_pane'  ]['weight'] = 4;
  $checkout_panes['commerce_user_profile_pane'  ][  'page'] = 'checkout';

  // Delivery method page.
  $checkout_panes['gc_checkout_summary_shipping']['weight'] = 0;
  $checkout_panes['gc_checkout_summary_shipping'][  'page'] = 'shipping';
  $checkout_panes['commerce_shipping'           ]['weight'] = 1;
  $checkout_panes['commerce_shipping'           ][  'page'] = 'shipping';

  if (arg(0) != 'admin') {
    $checkout_panes['gc_checkout_summary_shipping']['title'] = t('Summary');
    $checkout_panes['commerce_shipping']['title'] = '';
  }

  // Payment page.
  $checkout_panes['commerce_payment']['page'] = 'payment';

  // Completion page.
  $checkout_panes['checkout_completion_message']['page'] = 'complete';
}

/**
 * Implements hook_commerce_checkout_page_info_alter().
 */
function gc_checkout_commerce_checkout_page_info_alter(&$checkout_pages) {
  $checkout_pages['checkout']['title'] = t('Delivery information');
  $checkout_pages['shipping']['title'] = t('Delivery method');

  unset($checkout_pages['review']);

  // Force commerce_checkout_progress to show payment step.
  $checkout_pages['payment']['has_item'] = TRUE;
}

/**
 * Summary pane: form callback.
 */
function gc_checkout_summary_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  return array(
    '#theme' => 'gc_checkout_summary',
    '#order' => $order,
    '#attributes' => array(
      'class' => array(
        'gc-checkout-summary',
        'gc-checkout-summary--' . $checkout_pane['page'],
      ),
    ),
  );
}

/**
 * Google analytics pane: form callback.
 */
function gc_checkout_google_analytics_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Ensure we only send to google anaytics once.
  if (!empty($order->data['sent_to_google_analytics'])) {
    return array();
  }

  $order->data['sent_to_google_analytics'] = TRUE;
  commerce_order_save($order);

  $script = '';
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Get the transaction details...
  $transaction = array(
    'order_id' => $order_wrapper->order_number->value(),
    'store' => variable_get('site_name', 'Greene King Gift Cards'),
    'total' => 0,
    'tax' => 0,
    'shipping' => 0,
    'city' => '',
    'state' => '',
    'country' => '',
  );

  // ...total.
  $transaction['total'] = commerce_currency_amount_to_decimal(
    $order_wrapper->commerce_order_total->amount->value(),
    $order_wrapper->commerce_order_total->currency_code->value()
  );

  // ...shipping.
  foreach ($order_wrapper->commerce_line_items as $item) {
    if ($item->type->value() == 'shipping') {
      $transaction['shipping'] += commerce_currency_amount_to_decimal(
        $item->commerce_total->amount->value(),
        $item->commerce_total->currency_code->value()
      );
    }
  }

  // ...address.
  $profile = NULL;
  if (isset($order_wrapper->commerce_customer_billing) && $order_wrapper->commerce_customer_billing->value()) {
    $profile = commerce_customer_profile_load($order_wrapper->commerce_customer_billing->profile_id->value());
  }
  elseif (isset($order_wrapper->commerce_customer_shipping) && $order_wrapper->commerce_customer_shipping->value()) {
    $profile = commerce_customer_profile_load($order_wrapper->commerce_customer_shipping->profile_id->value());
  }

  if ($profile) {
    $profile_wrapper = entity_metadata_wrapper('commerce_customer_profile', $profile);
    if (isset($profile_wrapper->commerce_customer_address)) {
      include_once DRUPAL_ROOT . '/includes/locale.inc';
      $countries = country_get_list();
      $address = $profile_wrapper->commerce_customer_address->value();

      $transaction['city'] = $address['locality'];
      $transaction['state'] = $address['administrative_area'];
      $transaction['country'] = $countries[$address['country']];
    }
  }

  $script .= '_gaq.push(["_addTrans", ' . drupal_json_encode(array_values($transaction)) . "]);\n";

  // Get the line item details...
  foreach ($order_wrapper->commerce_line_items as $line_item) {
    $item = array(
      'order_id' => $order_wrapper->order_number->value(),
      'sku' => '',
      'name' => $line_item->line_item_label->value(),
      'category' => $line_item->type->value(),
      'price' => commerce_currency_amount_to_decimal(
        $line_item->commerce_unit_price->amount->value(),
        $line_item->commerce_unit_price->currency_code->value()
      ),
      'qty' => $line_item->quantity->value(),
    );

    // ...product specific details.
    if (isset($line_item->commerce_product)) {
      $item['sku'] = $line_item->commerce_product->sku->value();
      $item['name'] = $line_item->commerce_product->title->value();
    }
    // ...shipping specific values.
    elseif (isset($line_item->commerce_shipping_service)) {
      $item['sku'] = $line_item->commerce_shipping_service->value();
    }

    $script .= '_gaq.push(["_addItem", ' . drupal_json_encode(array_values($item)) . "]);\n";
  }

  $script .= '_gaq.push(["_trackTrans"]);';

  // Add the script.
  drupal_add_js($script, array(
    'type' => 'inline',
    'scope' => variable_get('googleanalytics_js_scope', 'footer'),
    'preprocess' => FALSE,
    'weight' => 10)
  );
}

/**
 * AJAX callback for the summary block.
 */
function gc_checkout_summary_js() {
  if (!empty($_GET['order_number']) && $order = commerce_order_load_by_number($_GET['order_number'])) {
    echo theme('gc_checkout_summary', array(
      'order' => $order,
    ));
  }

  exit;
}

/**
 * AJAX callback for checkout progress block.
 */
function gc_checkout_progress_js($page_id = 'payment') {
  print theme('commerce_checkout_progress_list', array(
    'items' => commerce_checkout_progress_get_items(),
    'type' => variable_get('commerce_checkout_progress_list_type', 'ol'),
    'current_page' => $page_id,
    'link' => variable_get('commerce_checkout_progress_link', TRUE),
  ));

  exit;
}
