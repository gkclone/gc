<?php
/**
 * @file
 * gc_checkout.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function gc_checkout_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'commerce_checkout_completion_message';
  $strongarm->value = array(
    'value' => '<div class="gk-commerce-checkout-complete-banner">
	<div class="gk-commerce-checkout-complete-banner-inner">
		<div class="gk-commerce-checkout-complete-banner-first-line">Thank You</div>

		<div class="gk-commerce-checkout-complete-banner-second-line">for your purchase</div>
	</div>
</div>

<p>Thank you for purchasing a Greene King gift card. Your order number is [commerce-order:order-number].&nbsp;An email will be sent to you shortly confirming your order.</p>
',
    'format' => 'full_html',
  );
  $export['commerce_checkout_completion_message'] = $strongarm;

  return $export;
}
