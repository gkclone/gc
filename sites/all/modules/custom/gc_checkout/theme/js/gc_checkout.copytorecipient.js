(function($) {

  Drupal.behaviors.gc_checkout_copytorecipient = {
    attach: function (context, settings) {
      $('.customer_profile_billing').append(
        $('<div />', { 'class': 'field-type-list-boolean copyinfo' }).append(
          $('<div />', { 'class': 'form-item' }).append(
            $('<div />', { 'class': 'form-item--field' }).append(
              $('<input>', {
                'type': 'checkbox',
                'class': 'form-checkbox',
                'id': 'checkbox_copyinfo'
              }),
              $('<label />', {
                'class': 'form-item__label--option',
                'text': 'Use this as the Recipient Address',
                for: 'checkbox_copyinfo'
              })
            )
          )
        )
      );

      $(document).on('change', '#checkbox_copyinfo', function(){
        if ($(this).is(':checked')) {
          billingValues = [];

           $('.customer_profile_billing')
          .find('input[type="text"], select')
          .not('checkbox')
          .each(function() {
            billingValues.push($(this).val());
          });

          var i = 0;

          $('.customer_profile_shipping').find('input[type=text], select').each(function() {
            $(this).val(billingValues[i]).trigger('change');

            i++;
          });
        }
      });
    }
  }

})(jQuery);
