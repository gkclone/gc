<div id="message" class="grid__cell">
  <div class="messages is-error">
    <h2 class="element-invisible"><?php print $label; ?></h2>
    <i class="messages__icon icon--exclamation-sign"></i>
    <?php print $message; ?>
  </div>
</div>
