<div class="line_items">
  <?php foreach ($line_items as $line_item): ?>
    <div class="line_item">
      <div class="line_item__image">
        <span>
          <?php echo $line_item['image']; ?>
        </span>
      </div>

      <?php if (!empty($line_item['fields'])): ?>
        <div class="fields">
          <?php foreach ($line_item['fields'] as $field): ?>
            <div class="fields__field">
              <div class="field__label">
                <?php echo $field['label']; ?>
              </div>
              <div class="field__value">
                <?php echo render($field['value']); ?>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>
  <?php endforeach; ?>
</div>

<?php if (!empty($shipping_address)): ?>
  <div class="shipping-address">
    <div class="field-label">Recipient<br>Address</div>
    <?php echo render($shipping_address); ?>
  </div>
<?php endif; ?>

<div class="order-total">
  <div class="field">
    <div class="field__label">
      Total
    </div>
    <div class="field__value">
      <?php echo $order_total; ?>
    </div>
  </div>
</div>
