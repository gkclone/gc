<?php
/**
 * @file
 * gc_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function gc_permissions_user_default_roles() {
  $roles = array();

  // Exported role: admin.
  $roles['admin'] = array(
    'name' => 'admin',
    'weight' => 2,
  );

  // Exported role: finance.
  $roles['finance'] = array(
    'name' => 'finance',
    'weight' => 3,
  );

  // Exported role: support.
  $roles['support'] = array(
    'name' => 'support',
    'weight' => 4,
  );

  return $roles;
}
