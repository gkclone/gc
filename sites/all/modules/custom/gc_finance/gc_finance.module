<?php

/**
 * Implements hook_menu().
 */
function gc_finance_menu() {
  $items['admin/commerce/reports'] = array(
    'title' => t('Reports'),
    'position' => 'right',
    'description' => t('Generate commerce reports.'),
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  $items['admin/commerce/reports/finance'] = array(
    'title' => t('Finance'),
    'description' => t('Generate a finance report.'),
    'page callback' => 'gc_finance_page_callback',
    'access arguments' => array('access commerce finance report'),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function gc_finance_permission() {
  return array(
    'access commerce finance report' => array(
      'title' => t('Access commerce finance report'),
    ),
  );
}

/**
 * Callback for the finance report page.
 */
function gc_finance_page_callback() {
  return array(
    'filter_form' => drupal_get_form('gc_finance_filter_form'),
  );
}

/**
 * A form for filtering orders.
 */
function gc_finance_filter_form() {
  $form['date_from'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#title' => t('From'),
  );

  $form['date_to'] = array(
    '#type' => 'date_popup',
    '#date_format' => 'Y-m-d',
    '#title' => t('To'),
  );

  global $user;
  if ($user->uid == 1) {
    $form['debug'] = array(
      '#type' => 'checkbox',
      '#title' => t('Debug'),
      '#default_value' => 1,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
  );

  return $form;
}

/**
 * Submit handler for the filter form.
 */
function gc_finance_filter_form_submit($form, &$form_state) {
  $orders = gc_finance_get_items(array(
    'filters' => array(
      'status' => array('pending', 'processing', 'completed'),
      'date_from' => $form_state['values']['date_from'],
      'date_to' => $form_state['values']['date_to'],
    ),
  ));

  if (empty($orders)) {
    drupal_set_message(t('No orders were found.'));
  }
  else {
    $content = gc_finance_export_file_content($orders);

    // Debug mode?
    if (!empty($form_state['values']['debug']) && module_exists('devel')) {
      dpm($content);

      // Generate some totals. First split the data back up into rows.
      $rows = array_map(
        function ($_) {
          return explode(',', $_);
        },
        explode("\n", $content)
      );

      dpm($count_rows = count($rows), 'Total cards');
      $sales_value_total = $shipping_value = $total_value_total = 0;

      // Skip the header row.
      for ($i = 1; $i < $count_rows; ++$i) {
        $row = array_combine(array_keys(gc_finance_csv_headers()), $rows[$i]);

        $sales_value_total += trim(substr($row['sales_value'], 1, -1), '£');
        $shipping_value_total += trim(substr($row['shipping_value'], 1, -1), '£');
        $total_value_total += trim(substr($row['total_value'], 1, -1), '£');
      }

      dpm($sales_value_total, 'Sales total');
      dpm($shipping_value_total, 'Shipping total');
      dpm($total_value_total, 'Grand total');
    }
    else {
      $filename = 'giftcards-finance';

      if (!empty($form_state['values']['date_from'])) {
        $filename .= '--from-' . $form_state['values']['date_from'];
      }

      if (!empty($form_state['values']['date_to'])) {
        $filename .= '--to-' . $form_state['values']['date_to'];
      }

      header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
      header('Content-Type: text/csv; charset=utf-8; encoding=utf-8');
      header('Content-Disposition: attachment; filename="' . $filename . '.csv"');

      die(utf8_decode($content));
    }
  }
}

/**
 * Retrieve orders that have associated commerce_payment_transaction entities.
 */
function gc_finance_get_items($options = array()) {
  $default_options = array(
    'filters' => array(
      'status' => NULL,
      'date_from' => NULL,
      'date_to' => NULL,
    ),
  );
  $options = gk_core_options_extend($default_options, $options);

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'commerce_order')
    ->propertyOrderBy('created', 'DESC')
  ;

  if (isset($options['filters']['status'])) {
    $query->propertyCondition('status', $options['filters']['status']);
  }

  $date_metadata = array();

  if (isset($options['filters']['date_from'])) {
    $date_metadata['date_from'] = $options['filters']['date_from'];
  }

  if (isset($options['filters']['date_to'])) {
    $date_metadata['date_to'] = $options['filters']['date_to'];
  }

  if (!empty($date_metadata)) {
    $query->addTag('gc_finance_date_filter');
    $query->addMetaData('date_filter', $date_metadata);
  }

  $result = $query->execute();

  if (!empty($result['commerce_order'])) {
    return commerce_order_load_multiple(array_keys($result['commerce_order']));
  }

  return array();
}

/**
 * Implements hook_query_TAG_alter().
 */
function gc_finance_query_gc_finance_date_filter_alter(QueryAlterableInterface $query) {
  if ($metadata = $query->getMetaData('date_filter')) {
    if (!empty($metadata['date_from'])) {
      $query->join('commerce_payment_transaction', 'pt_date_from', 'commerce_order.order_id = pt_date_from.order_id');
      $query->condition('pt_date_from.created', strtotime($metadata['date_from']), '>=');
    }

    if (!empty($metadata['date_to'])) {
      $query->join('commerce_payment_transaction', 'pt_date_to', 'commerce_order.order_id = pt_date_to.order_id');
      $query->condition('pt_date_to.created', strtotime($metadata['date_to']), '<=');
    }
  }
}

/**
 * Generate CSV content given a list of commerce_order entities.
 */
function gc_finance_export_file_content($orders) {
  $order_ids = array();

  foreach ($orders as $order) {
    $order_ids[] = $order->order_id;
  }

  // Load the commerce_payment_transaction entities and ensure they are keyed by
  // order ID.
  $payment_transaction_entites = commerce_payment_transaction_load_multiple(array(), array('order_id' => $order_ids));
  $payment_transactions = array();

  foreach ($payment_transaction_entites as $payment_transaction) {
    $payment_transactions[$payment_transaction->order_id] = $payment_transaction;
  }

  // Load the commerce_line_item entities and ensure they are keyed by order ID.
  $line_item_entities = commerce_line_item_load_multiple(array(), array('order_id' => $order_ids));
  $line_items = array();

  foreach ($line_item_entities as $line_item) {
    $line_items[$line_item->order_id][] = $line_item;
  }

  $line_item_entities = $line_items;

  // Build each row in the CSV. Each row represents one order.
  $csv_rows = array();

  foreach ($orders as $order) {
    $payment_transaction = $payment_transactions[$order->order_id];
    $line_items = $line_item_entities[$order->order_id];

    // Find the value of the gift card and shipping sales.
    $giftcard_sales_value = $shipping_sales_value = 0;

    foreach ($line_items as $line_item) {
      $varname = $line_item->type . '_sales_value';
      $$varname += $line_item->commerce_total[LANGUAGE_NONE][0]['amount'];
    }

    $total_sales_value = $payment_transaction->amount;
    $currency = $payment_transaction->currency_code;

    $csv_row = array(
      'transaction_date' => date('Y-m-d', $payment_transaction->created),
      'transaction_time' => date('H:i:s', $payment_transaction->created),
      'transaction_id'   => $payment_transaction->remote_id,
      'order_number'     => $order->order_number,
      'sales_value'      => commerce_currency_format($giftcard_sales_value, $currency),
      'shipping_value'   => commerce_currency_format($shipping_sales_value, $currency),
      'total_value'      => commerce_currency_format($total_sales_value, $currency),
    );

    $csv_rows[] = '"' . implode('","', $csv_row) . '"';
  }

  $csv_headers = array('"' . implode('","', gc_finance_csv_headers()) . '"');
  return implode("\n", array_merge($csv_headers, $csv_rows));
}

/**
 * Return the header row for the CSV export files.
 */
function gc_finance_csv_headers() {
  return array(
    'transaction_date' => 'Transaction date',
    'transaction_time' => 'Transaction time',
    'transaction_id'   => 'Transaction ID',
    'order_number'     => 'Order number',
    'sales_value'      => 'Sales value',
    'shipping_value'   => 'Postage charge',
    'total_value'      => 'Total value',
  );
}
