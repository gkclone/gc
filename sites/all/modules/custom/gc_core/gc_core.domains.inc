<?php
/**
 * @file
 * gc_core.domains.inc
 */

/**
 * Implements hook_domain_default_domains().
 */
function gc_core_domain_default_domains() {
  $domains = array();
  $domains['gkgiftcards_belhaven'] = array(
    'subdomain' => 'belhaven.gkgiftcards.co.uk',
    'sitename' => 'Belhaven Gift Cards',
    'scheme' => 'https',
    'valid' => 1,
    'weight' => 1,
    'is_default' => 0,
    'machine_name' => 'gkgiftcards_belhaven',
  );
  $domains['gkgiftcards_default'] = array(
    'subdomain' => 'www.gkgiftcards.co.uk',
    'sitename' => 'Greene King Gift Cards',
    'scheme' => 'https',
    'valid' => 1,
    'weight' => -1,
    'is_default' => 1,
    'machine_name' => 'gkgiftcards_default',
  );
  $domains['gkgiftcards_hungryhorse'] = array(
    'subdomain' => 'hungryhorse.gkgiftcards.co.uk',
    'sitename' => 'Hungry Horse Gift Cards',
    'scheme' => 'https',
    'valid' => 1,
    'weight' => 1,
    'is_default' => 0,
    'machine_name' => 'gkgiftcards_hungryhorse',
  );
  $domains['gkgiftcards_lochfyne'] = array(
    'subdomain' => 'lochfyne.gkgiftcards.co.uk',
    'sitename' => 'Loch Fyne Seafood & Grill Gift Cards',
    'scheme' => 'https',
    'valid' => 1,
    'weight' => 1,
    'is_default' => 0,
    'machine_name' => 'gkgiftcards_lochfyne',
  );

  return $domains;
}

/**
 * Implements hook_domain_alias_default_aliases().
 */
function gc_core_domain_alias_default_aliases() {
  $domain_aliases = array();
  $domain_aliases['gkgiftcards_belhaven'] = array(
    'belhaven.giftcards.local' => array(
      'pattern' => 'belhaven.giftcards.local',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_belhaven',
    ),
    'belhaven.giftcards.local:8080' => array(
      'pattern' => 'belhaven.giftcards.local:8080',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_belhaven',
    ),
    'belhaven.giftcards.greenekingdigital.co.uk' => array(
      'pattern' => 'belhaven.giftcards.greenekingdigital.co.uk',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_belhaven',
    ),
  );
  $domain_aliases['gkgiftcards_default'] = array(
    'giftcards.local' => array(
      'pattern' => 'giftcards.local',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_default',
    ),
    'giftcards.local:8080' => array(
      'pattern' => 'giftcards.local:8080',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_default',
    ),
    'giftcards.greenekingdigital.co.uk' => array(
      'pattern' => 'giftcards.greenekingdigital.co.uk',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_default',
    ),
  );
  $domain_aliases['gkgiftcards_hungryhorse'] = array(
    'hungryhorse.giftcards.local' => array(
      'pattern' => 'hungryhorse.giftcards.local',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_hungryhorse',
    ),
    'hungryhorse.giftcards.local:8080' => array(
      'pattern' => 'hungryhorse.giftcards.local:8080',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_hungryhorse',
    ),
    'hungryhorse.giftcards.greenekingdigital.co.uk' => array(
      'pattern' => 'hungryhorse.giftcards.greenekingdigital.co.uk',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_hungryhorse',
    ),
  );
  $domain_aliases['gkgiftcards_lochfyne'] = array(
    'lochfyne.giftcards.local' => array(
      'pattern' => 'lochfyne.giftcards.local',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_lochfyne',
    ),
    'lochfyne.giftcards.local:8080' => array(
      'pattern' => 'lochfyne.giftcards.local:8080',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_lochfyne',
    ),
    'lochfyne.giftcards.greenekingdigital.co.uk' => array(
      'pattern' => 'lochfyne.giftcards.greenekingdigital.co.uk',
      'redirect' => 0,
      'machine_name' => 'gkgiftcards_lochfyne',
    ),
  );

  return $domain_aliases;
}

/**
 * Implements hook_domain_theme_default_themes().
 */
function gc_core_domain_theme_default_themes() {
  $domain_themes = array();
  $domain_themes['gkgiftcards_belhaven'] = array(
    'belhaven' => array(
      'theme' => 'belhaven',
      'settings' => NULL,
      'status' => 1,
      'filepath' => '',
    ),
  );
  $domain_themes['gkgiftcards_default'] = array();
  $domain_themes['gkgiftcards_hungryhorse'] = array(
    'hungryhorse' => array(
      'theme' => 'hungryhorse',
      'settings' => NULL,
      'status' => 1,
      'filepath' => '',
    ),
  );
  $domain_themes['gkgiftcards_lochfyne'] = array(
    'lochfyne' => array(
      'theme' => 'lochfyne',
      'settings' => NULL,
      'status' => 1,
      'filepath' => '',
    ),
  );

  return $domain_themes;
}
